﻿using System;
using UnityEngine;

public class DoorInteraction : MonoBehaviour {

    [Serializable]
    public class DoorInteractionSettings
    {
        public float smooth = 2f;
        public float doorOpenAngle = 90f;
        public float doorCloseAngle = 0f;
    }

    public DoorInteractionSettings settings = new DoorInteractionSettings();

    private bool isOpen;
    private bool isOpening;

    // Use this for initialization
    void Start () {
        isOpen = false;
        isOpening = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

    // This function will add the force corresponding to the horizontal rotation
    public void rotateDoor(Vector3 force)
    {
        float d = force.magnitude;

        
    }
}
