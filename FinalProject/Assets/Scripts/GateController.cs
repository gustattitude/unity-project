﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour {

    public GameObject gate;
    public bool gateIsOpening;
    public Animation anim;
	
	// Update is called once per frame
	void Update () {
		if(gateIsOpening){
			gate.transform.Translate(Vector3.up * Time.deltaTime * 5);
		}
		if(gate.transform.position.y > 5f){
			gateIsOpening = false;
		}
	}
	void OnMouseDown(){ //detect the mouse click on collider of button
		gateIsOpening = true;

	}
}
