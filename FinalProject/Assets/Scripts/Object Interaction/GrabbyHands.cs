﻿using System;
using UnityEngine;



[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class GrabbyHands : MonoBehaviour {

    [Serializable]
    public class GrabbHandsSettings
    {
        public float velocityModifier;
        public float grabDistance;
    }

    public GrabbHandsSettings settings = new GrabbHandsSettings();

    public SixenseHand m_hand;
    private GameObject m_inHand = null;
    private Transform m_inHandParent = null;
    private Vector3 m_previousPos;
    private Vector3 m_deltaPos;

    private JNT_Grab_S JNT_Grab;



	// Use this for initialization
	void Start () {
        m_hand = GetComponent<SixenseHand>();
        JNT_Grab = GetComponentInChildren<JNT_Grab_S>();

        m_previousPos = m_hand.transform.position;
        m_deltaPos = new Vector3(0, 0, 0);
	}

    // Update is called once per frame
    void Update() {
        m_deltaPos = m_hand.transform.position - m_previousPos;
        m_previousPos = m_hand.transform.position;

        ////Ray Cast in palm direction
        //if(m_hand.isActiveAndEnabled && m_inHand == null)
        //{
        //    //Debug.DrawRay(JNT_Grab.gameObject.transform.position, -JNT_Grab.gameObject.transform.up);

        //    RaycastHit hit;
        //    if(Physics.Raycast(JNT_Grab.gameObject.transform.position, -JNT_Grab.gameObject.transform.up, out hit, settings.grabDistance))
        //    {
        //        if (m_hand == null || m_hand.m_controller == null)
        //        {
        //            return;
        //        }

        //        if(m_hand.m_controller.Trigger >= m_hand.m_controller.TriggerButtonThreshold)
        //        {
        //            GameObject target = hit.collider.gameObject;
        //            //Door Grabbing Part
        //            if(target.CompareTag("DoorKnot"))
        //            {
        //                dragDoor();
        //            }
        //        }
                
        //    }
        //}
    }

    void dragDoor(GameObject doorKnot)
    {
        // Take the door hallway object
        DoorInteraction doorFrame = doorKnot.transform.parent.transform.parent.GetComponent<DoorInteraction>();

        // Compute the rotation
        doorFrame.rotateDoor(m_deltaPos);
    }

    void OnTriggerEnter(Collider target)
    {
        if (m_hand == null || m_hand.m_controller == null)
        {
            return;
        }

        if (m_hand.isActiveAndEnabled && m_hand.m_controller.Trigger >= m_hand.m_controller.TriggerButtonThreshold)
        {

            // Check whether the collided object can be picked up
            if (target.CompareTag("Pickupable"))
            {
                //Grab the object
                m_inHand = target.gameObject;
                m_inHand.GetComponent<Rigidbody>().isKinematic = true;
                m_inHand.GetComponent<Rigidbody>().useGravity = false;
                m_inHandParent = m_inHand.transform.parent;
                m_inHand.transform.parent = m_hand.transform;

                // Can't open the door if the hand is already holding smth
            } else if(target.CompareTag("DoorKnot") && m_inHand == null)
            {
                dragDoor(target.gameObject);
            }
        }
    }

    void OnTriggerExit(Collider target)
    {
        if (m_hand == null || m_hand.m_controller == null)
        {
            return;
        }

        if (m_inHand != null)
        {
            if (m_hand.isActiveAndEnabled && m_hand.m_controller.Trigger < 1)
            {
                // unparent it from the hand by restoring its old parent
                m_inHand.transform.parent = m_inHandParent;
                m_inHand.GetComponent<Rigidbody>().isKinematic = false;
                m_inHand.GetComponent<Rigidbody>().useGravity = true;
                m_inHand.GetComponent<Rigidbody>().velocity = (m_deltaPos * settings.velocityModifier);
                m_inHand = null;
            }
        }
    }

    //void OnCollisionStay(Collision collision)
    //{
    //    if(m_hand == null || m_hand.m_controller == null)
    //    {
    //        return;
    //    }

    //    if(m_hand.isActiveAndEnabled && m_hand.m_controller.Trigger >= m_hand.m_controller.TriggerButtonThreshold)
    //    {
            
    //        // Check whether the collided object can be picked up
    //        if(collision.gameObject.CompareTag("Pickupable"))
    //        {
    //            //Grab the object
    //            m_inHand = collision.gameObject;
    //            m_inHand.GetComponent<Rigidbody>().isKinematic = true;
    //            m_inHand.GetComponent<Rigidbody>().useGravity = false;
    //            m_inHandParent = m_inHand.transform.parent;
    //            m_inHand.transform.parent = m_hand.transform;
    //        }
    //    }
    //}

    //void FixedUpdate()
    //{
    //    print("fixedUpdate");

    //    if (m_hand == null || m_hand.m_controller == null)
    //    {
    //        return;
    //    }

    //    if (m_inHand != null)
    //    {
    //        if(m_hand.isActiveAndEnabled && m_hand.m_controller.Trigger < 1)
    //        {
    //            // unparent it from the hand by restoring its old parent
    //            m_inHand.transform.parent = m_inHandParent;
    //            m_inHand.GetComponent<Rigidbody>().isKinematic = false;
    //            m_inHand.GetComponent<Rigidbody>().useGravity = true;
    //            m_inHand.GetComponent<Rigidbody>().velocity = (m_deltaPos * settings.velocityModifier);
    //            m_inHand = null;
    //        }
    //    }
    //}
}
