﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlateController : MonoBehaviour {

    public GameObject gate;
    public bool gateIsOpening;
    
    public static bool redStone;
    public static bool blueStone;
    public static bool greenStone;
    private float startingY;
	
	// Update is called once per frame
	void Start () {
		startingY = gate.transform.position.y;
	}
	void Update () {
		if(redStone && blueStone && greenStone){
			gateIsOpening = true;
			if(gate.transform.position.y > 3f){
			gateIsOpening = false;
			}
			if(gateIsOpening){
			gate.transform.Translate(Vector3.right * Time.deltaTime * 5);
			}
			
		} else {
			gateIsOpening = true;
			if(gate.transform.position.y <= startingY){
			gateIsOpening = false;
			} 
			if(gateIsOpening){
			gate.transform.Translate(Vector3.left * Time.deltaTime * 5);
			}
			

		}
	}
}
