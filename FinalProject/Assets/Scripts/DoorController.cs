﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {

    public GameObject Door_1;
    public bool doorIsOpening;

	
	// Update is called once per frame
	void Update () {
		if(doorIsOpening){
			Door_1.transform.Translate(Vector3.up * Time.deltaTime * 5);
		}
		if(Door_1.transform.position.y > 7f){
			doorIsOpening = false;
		}
	}
	void OnMouseDown(){ //detect the mouse click on collider of button
		doorIsOpening = true;

	}
}
